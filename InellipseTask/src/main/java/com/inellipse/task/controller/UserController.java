package com.inellipse.task.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inellipse.task.dto.UserPersonDto;
import com.inellipse.task.model.User;
import com.inellipse.task.service.impl.UserServiceImpl;

@RestController
@RequestMapping("/api/users")
@CrossOrigin
public class UserController {
	
	@Autowired
	private UserServiceImpl userServiceImpl;
	

	@PostMapping
	public User addUser(@RequestBody User user) {
		return userServiceImpl.createUser(user);
	}

	@GetMapping("/{id}")
	public User getUser(@PathVariable("id") Integer id) {
		return userServiceImpl.getUser(id);
	}

	@GetMapping
	public List<User> getUsers() {
		return userServiceImpl.getUsers();
	}

	@DeleteMapping("/{id}")
	public Boolean deleteUser(@PathVariable("id") Integer id) {
		userServiceImpl.deleteUser(id);
		return true;
	}
	
	@PutMapping("/follow")
	public User followPerson(@RequestBody UserPersonDto dto) {
		return userServiceImpl.followPerson(dto);
	}
	
	@PutMapping("/unfollow")
	public User unfollowPerson(@RequestBody UserPersonDto dto) {
		return userServiceImpl.unfollowPerson(dto);
	}

}
