package com.inellipse.task.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.inellipse.task.dto.PersonVideoDto;
import com.inellipse.task.model.People;
import com.inellipse.task.service.impl.PeopleServiceImpl;

@RestController
@RequestMapping("/api/people")
@CrossOrigin
public class PeopleController {
	
	@Autowired
	private PeopleServiceImpl peopleServiceImpl;
	

	@PostMapping
	public People addPeople(@RequestBody People people) {
		return peopleServiceImpl.createPeople(people);
	}

	@GetMapping("/{id}")
	public People getPeople(@PathVariable("id") Integer id) {
		return peopleServiceImpl.getPeople(id);
	}

	@GetMapping
	public List<People> getPeople() {
		return peopleServiceImpl.getPeoples();
	}

	@DeleteMapping("/{id}")
	public Boolean deleteUser(@PathVariable("id") Integer id) {
		peopleServiceImpl.deletePeople(id);
		return true;
	}
	
	@GetMapping("/getSuggested/{id}")
	public List<People> getSuggested(@PathVariable("id") Integer id){
		return peopleServiceImpl.getSuggested(id);
	}
	
	@GetMapping("/getPersonByVideo/{id}")
	public People getPersonByVideo(@PathVariable("id") Integer id){
		return peopleServiceImpl.getPersonByVideo(id);
	}
	
	
	
}
