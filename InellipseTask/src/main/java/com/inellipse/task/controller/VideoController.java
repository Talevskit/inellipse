package com.inellipse.task.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.inellipse.task.dto.AddComment;
import com.inellipse.task.dto.PersonVideoDto;
import com.inellipse.task.dto.VideoAuthor;
import com.inellipse.task.model.People;
import com.inellipse.task.model.Video;
import com.inellipse.task.service.impl.CommentServiceImpl;
import com.inellipse.task.service.impl.VideoServiceImpl;

@RestController
@RequestMapping("/api/videos")
@CrossOrigin
public class VideoController {
	
	@Autowired
	private VideoServiceImpl videoServiceImpl;
	@Autowired
	private CommentServiceImpl commentServiceImpl;
	

	@PostMapping
	public People addVideo(@RequestBody PersonVideoDto dto) {
		return videoServiceImpl.createVideo(dto);
	}

	@GetMapping("/{id}")
	public Video getVideo(@PathVariable("id") Integer id) {
		return videoServiceImpl.getVideo(id);
	}
	
	@GetMapping("/getVideosWithAuthor/{id}")
	public List<VideoAuthor> getVideosWithAuthor(@PathVariable("id") Integer id) {
		return videoServiceImpl.getVideosWithAuthor(id);
	}
	
	@GetMapping("getFollowingVideos/{id}")
	public List<Video> getFollowingVideos(@PathVariable("id") Integer id) {
		return videoServiceImpl.getFollowingVideos(id);
	}

	@GetMapping
	public List<Video> getVideos() {
		return videoServiceImpl.getVideos();
	}
	@GetMapping("/all/{id}")
	public List<VideoAuthor> getVideos(@PathVariable("id") Integer id) {
		return videoServiceImpl.getAllVideosWithAuthor(id);
	}

	@DeleteMapping("/{id}")
	public Boolean deleteVideo(@PathVariable("id") Integer id) {
		videoServiceImpl.deleteVideo(id);
		return true;
	}
	
	@DeleteMapping("/deleteComment/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void removeComment(@PathVariable("id") Integer id) {
		commentServiceImpl.removeComment(id);
	}
	
	@PutMapping("/addComment")
	public Video addComment(@RequestBody AddComment addComment) {
		return commentServiceImpl.addComment(addComment);
	}

}
