package com.inellipse.task.dto;

import lombok.Data;

@Data
public class UserPersonDto {
	
	private Integer userId;
	private Integer peopleId;

}
