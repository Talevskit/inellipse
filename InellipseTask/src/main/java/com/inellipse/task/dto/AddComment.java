package com.inellipse.task.dto;


import com.inellipse.task.model.Comment;

import lombok.Data;

@Data
public class AddComment {
	private Integer id;
	private Comment comment;

}