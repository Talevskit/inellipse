package com.inellipse.task.dto;

import com.inellipse.task.model.Video;

import lombok.Data;

@Data
public class PersonVideoDto {
	
	private Integer personId;
	private Video video;

}
