package com.inellipse.task.dto;

import com.inellipse.task.model.People;
import com.inellipse.task.model.Video;

import lombok.Data;

@Data
public class VideoAuthor {
	
	private People person;
	private Video video;

}
