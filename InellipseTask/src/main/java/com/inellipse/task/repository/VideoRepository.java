package com.inellipse.task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.inellipse.task.model.Video;

@Repository
public interface VideoRepository extends JpaRepository<Video, Integer>{

}
