package com.inellipse.task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.inellipse.task.model.Comment;


@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer>{

}
