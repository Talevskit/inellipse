package com.inellipse.task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.inellipse.task.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

}
