package com.inellipse.task.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.inellipse.task.model.User;

@Service
public interface UserService {
	
	public User getUser(Integer id);
	public List<User> getUsers();
	public User createUser(User user);
	public Boolean deleteUser(Integer id);
	public User updateUser(User user, Integer id);

}
