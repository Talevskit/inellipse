package com.inellipse.task.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inellipse.task.model.People;
import com.inellipse.task.model.User;
import com.inellipse.task.model.Video;
import com.inellipse.task.repository.PeopleRepository;
import com.inellipse.task.repository.UserRepository;
import com.inellipse.task.repository.VideoRepository;
import com.inellipse.task.service.PeopleService;

@Service
public class PeopleServiceImpl implements PeopleService{
	
	@Autowired
	private PeopleRepository peopleRepository;
	@Autowired
	private VideoRepository videoRepository;
	@Autowired
	private UserRepository userRepository;

	@Override
	public People getPeople(Integer id) {
		return peopleRepository.getOne(id);
	}

	@Override
	public List<People> getPeoples() {
		return peopleRepository.findAll();
	}

	@Override
	public People createPeople(People people) {
		People newPeople = new People();
		newPeople.setFirstName(people.getFirstName());
		newPeople.setLastName(people.getLastName());
		return peopleRepository.save(newPeople);
	}

	@Override
	public Boolean deletePeople(Integer id) {
		peopleRepository.deleteById(id);
		return true;
	}

	@Override
	public People updatePeople(People people, Integer id) {
		// TODO Auto-generated method stub
		return null;
	}
	
//	public List<People> getFollowing(Integer id){
//		User user = userRepository.getOne(id);
//		List<People> suggested = new ArrayList<People>();
//
//	}
	
	public List<People> getSuggested(Integer id){
		User user = userRepository.getOne(id);
		List<People> suggested = new ArrayList<People>();
		peopleRepository.findAll().stream().forEach(x-> {
			if(!(user.getFollowing().contains(x)))
				suggested.add(x);
		});
		return suggested;
	}

	public People getPersonByVideo(Integer id) {
		Video video = videoRepository.findById(id).get();
		People people = video.getPeople();
		return people;
	}
	
}
