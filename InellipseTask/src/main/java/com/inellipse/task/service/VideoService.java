package com.inellipse.task.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.inellipse.task.dto.PersonVideoDto;
import com.inellipse.task.model.People;
import com.inellipse.task.model.Video;

@Service
public interface VideoService {
	
	public Video getVideo(Integer id);
	public List<Video> getVideos();
	public People createVideo(PersonVideoDto dto);
	public Boolean deleteVideo(Integer id);
	public Video updateVideo(Video video, Integer id);

}
