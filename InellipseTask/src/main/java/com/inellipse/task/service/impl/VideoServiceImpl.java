package com.inellipse.task.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inellipse.task.dto.PersonVideoDto;
import com.inellipse.task.dto.VideoAuthor;
import com.inellipse.task.model.People;
import com.inellipse.task.model.User;
import com.inellipse.task.model.Video;
import com.inellipse.task.repository.PeopleRepository;
import com.inellipse.task.repository.UserRepository;
import com.inellipse.task.repository.VideoRepository;
import com.inellipse.task.service.VideoService;

@Service
public class VideoServiceImpl implements VideoService{
	
	@Autowired
	private VideoRepository videoRepository;
	@Autowired
	private PeopleRepository peopleRepository;
	@Autowired
	private UserRepository userRepository;

	@Override
	public Video getVideo(Integer id) {
		return videoRepository.getOne(id);
	}

	@Override
	public List<Video> getVideos() {
		return videoRepository.findAll();
	}

	@Override
	public People createVideo(PersonVideoDto dto) {
		Video newVideo = new Video();
		newVideo.setTitle(dto.getVideo().getTitle());
		newVideo.setDescription(dto.getVideo().getDescription());
		newVideo.setUrl(dto.getVideo().getUrl());
		People people = peopleRepository.getOne(dto.getPersonId());
		newVideo.setPeople(people);
		people.getVideos().add(newVideo);
		videoRepository.save(newVideo);
		return people;
	}
	
	public List<Video> getFollowingVideos(Integer id){
		List<Video> videos = new ArrayList<Video>();
		User user = userRepository.getOne(id);
		user.getFollowing().stream().forEach(x-> {
			x.getVideos().stream().forEach(y -> {
				videos.add(y);
			});
		});
		return videos;
	}
	
	public List<VideoAuthor> getVideosWithAuthor(Integer id){
		List<VideoAuthor> videos = new ArrayList<VideoAuthor>();
		User user = userRepository.getOne(id);
		user.getFollowing().stream().forEach(x-> {
			x.getVideos().stream().forEach(y -> {
				VideoAuthor video = new VideoAuthor();
			video.setPerson(x);
			video.setVideo(y);
			videos.add(video);
			});
			
		});
		return videos;
		
	}
	public List<VideoAuthor> getAllVideosWithAuthor(Integer id){
		List<VideoAuthor> videos = new ArrayList<VideoAuthor>();
		videoRepository.findAll().stream().forEach(x-> {
			VideoAuthor video = new VideoAuthor();
			video.setPerson(x.getPeople());
			video.setVideo(x);
			videos.add(video);
		});
		return videos;
	}
	

	@Override
	public Boolean deleteVideo(Integer id) {
		videoRepository.deleteById(id);
		return true;
	}

	@Override
	public Video updateVideo(Video video, Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

}
