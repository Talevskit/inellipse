package com.inellipse.task.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.inellipse.task.model.People;

@Service
public interface PeopleService {
	
	public People getPeople(Integer id);
	public List<People> getPeoples();
	public People createPeople(People people);
	public Boolean deletePeople(Integer id);
	public People updatePeople(People people, Integer id);

}
