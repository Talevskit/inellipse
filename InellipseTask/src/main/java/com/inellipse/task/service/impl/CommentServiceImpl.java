package com.inellipse.task.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inellipse.task.dto.AddComment;
import com.inellipse.task.model.Comment;
import com.inellipse.task.model.Video;
import com.inellipse.task.repository.CommentRepository;
import com.inellipse.task.repository.VideoRepository;
import com.inellipse.task.service.CommentService;


@Service
public class CommentServiceImpl implements CommentService{
	@Autowired
	private CommentRepository commentRepository;
	@Autowired
	private VideoRepository videoRepository;

	@Override
	public Comment findById(Integer id) {
		return commentRepository.findById(id).get();
	}

	@Override
	public Video addComment(AddComment addComment) {
		Comment comment = new Comment();
		comment.setFirstName(addComment.getComment().getFirstName());
		comment.setComment(addComment.getComment().getComment());
		Video video = videoRepository.findById(addComment.getId()).get();
		comment.setVideo(video);
		video.getComments().add(comment);
		videoRepository.save(video);
		return video;
	}

	@Override
	public Comment updateComment(Integer id, Comment comment) {
		Comment updated = commentRepository.findById(id).get();
		updated.setFirstName(comment.getFirstName());
		updated.setComment(comment.getComment());
		updated.setVideo(videoRepository.findById(comment.getVideo().getId()).get());
		return commentRepository.save(updated);
	}

	@Override
	public List<Comment> getAll() {
		return commentRepository.findAll();
	}

	@Override
	public void removeComment(Integer id) {
		commentRepository.deleteById(id);
		
	}
	
	

}
