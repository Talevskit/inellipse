package com.inellipse.task.service;

import java.util.List;
import org.springframework.stereotype.Service;

import com.inellipse.task.dto.AddComment;
import com.inellipse.task.model.Comment;
import com.inellipse.task.model.Video;

@Service
public interface CommentService {
	
	public Comment findById(Integer id);

	public Video addComment(AddComment addComment);

	public Comment updateComment(Integer id, Comment comment);

	public List<Comment> getAll();
	
	public void removeComment(Integer id);

}
