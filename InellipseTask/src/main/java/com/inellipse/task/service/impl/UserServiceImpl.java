package com.inellipse.task.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inellipse.task.dto.UserPersonDto;
import com.inellipse.task.model.People;
import com.inellipse.task.model.User;
import com.inellipse.task.repository.PeopleRepository;
import com.inellipse.task.repository.UserRepository;
import com.inellipse.task.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PeopleRepository peopleRepository;

	@Override
	public User getUser(Integer id) {
		return userRepository.getOne(id);
	}

	@Override
	public List<User> getUsers() {
		return userRepository.findAll();
	}
	

	@Override
	public User createUser(User user) {
		User newUser = new User();
		newUser.setFirstName(user.getFirstName());
		newUser.setLastName(user.getLastName());
		newUser.setFollowing(user.getFollowing());
		return userRepository.save(newUser);
	}

	@Override
	public Boolean deleteUser(Integer id) {
		userRepository.deleteById(id);
		return true;
	}

	@Override
	public User updateUser(User user, Integer id) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public User followPerson(UserPersonDto dto) {
		User user = userRepository.findById(dto.getUserId()).get();
		People people = peopleRepository.getOne(dto.getPeopleId());
		user.getFollowing().add(people);
		return userRepository.save(user);
	}
	
	public User unfollowPerson(UserPersonDto dto) {
		User user = userRepository.findById(dto.getUserId()).get();
		People people = peopleRepository.getOne(dto.getPeopleId());
		user.getFollowing().remove(people);
		return userRepository.save(user);
	}

	
	

}
