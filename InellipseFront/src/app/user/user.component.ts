import { Component, OnInit, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Person } from '../models/person';
import { PersonVideo } from '../models/personVideo';
import { User } from '../models/user';
import { UserService } from '../service/user.service';
import { VideoService } from '../service/video.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit {
  public videos!: PersonVideo[];
  public personVideo!: PersonVideo[];
  public user: User;
  public persons: Person[];
  id: number;
  private sub: any;

  constructor(
    private VideoService: VideoService,
    private route: ActivatedRoute,
    private router: Router,
    private UserService: UserService
  ) {}

  ngOnChanges(): void {}

  ngOnInit(): void {
    this.sub = this.route.params.subscribe((params) => {
      this.id = +params['id'];
    });
    this.getUser(this.id);
    this.getSuggested(this.id);
    this.getAllVideos(this.id);
    this.getVideosWithAuthor(this.id);
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  public getVideos(): void {
    this.VideoService.getVideos().subscribe((res: PersonVideo[]) => {
      this.videos = res;
      console.log(this.videos);
    });
  }
  public getAllVideos(id): void {
    this.VideoService.getAllVideos(id).subscribe((res: any[]) => {
      this.videos = res;
      console.log(this.videos);
    });
  }
  public getUser(id): void {
    this.UserService.getUser(id).subscribe((res: User) => {
      this.user = res;
      console.log(this.user);
      localStorage.setItem('id', id);
      localStorage.setItem('firstName', this.user.firstName);
    });
  }
  public getVideosWithAuthor(id): void {
    this.VideoService.getVideosWithAuthor(id).subscribe(
      (res: PersonVideo[]) => {
        this.personVideo = res;
        console.log(this.personVideo);
      }
    );
  }
  public getSuggested(id): void {
    this.UserService.getSuggested(id).subscribe((res: Person[]) => {
      this.persons = res;
      console.log(this.persons);
    });
  }
  public userChangeHandler(user: User) {
    this.user = user;
    console.log(user);
  }
  public personsChangeHandler(persons: Person[]) {
    this.persons = persons;
    console.log(persons);
  }
}
