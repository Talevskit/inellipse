import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonVideo } from '../models/personVideo';
import { UserService } from '../service/user.service';
import { VideoService } from '../service/video.service';

@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.css'],
})
export class LiveComponent implements OnInit {
  @Input() videos: PersonVideo[];

  constructor(
    private VideoService: VideoService,
    private route: ActivatedRoute,
    private router: Router,
    private UserService: UserService
  ) {}

  ngOnInit(): void {}

  public getVideosWithAuthor(id): void {
    this.VideoService.getVideosWithAuthor(id).subscribe(
      (res: PersonVideo[]) => {
        this.videos = res;
        console.log(this.videos);
      }
    );
  }
}
