import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Comment } from '../models/comment';
import { Person } from '../models/person';
import { PersonVideo } from '../models/personVideo';
import { Video } from '../models/videos';
import { UserService } from '../service/user.service';
import { VideoService } from '../service/video.service';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css'],
})
export class VideoComponent implements OnInit {
  public videos!: PersonVideo[];
  public video: Video;
  public person: Person;
  id: number;
  private sub: any;

  constructor(
    private VideoService: VideoService,
    private UserService: UserService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe((params) => {
      this.id = +params['id'];
      this.getVideo();
      this.getVideos();
      this.getPersonByVideo(this.id);
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  public getVideo(): void {
    this.VideoService.getVideo(this.id).subscribe((res: Video) => {
      this.video = res;
      console.log(this.video);
    });
  }
  public getVideos(): void {
    this.VideoService.getVideos().subscribe((res: PersonVideo[]) => {
      this.videos = res;
      console.log(this.videos);
    });
  }
  public getPersonByVideo(id): void {
    this.UserService.getPersonByVideo(id).subscribe((res: Person) => {
      this.person = res;
      console.log(this.person);
    });
  }

  public watchVideo(id) {
    this.id = id;
    this.router.navigateByUrl(`/videos/${this.id}`);
    this.ngOnInit();
  }

  public addComment(comment: NgForm, id) {
    let obj = {
      id: id,
      comment: {
        firstName: localStorage.getItem('firstName'),
        comment: comment.value.comment,
      },
    };
    this.VideoService.addComment(obj).subscribe((res: any) => {
      console.log(res);
    });
    console.log(obj);
    let newOne: Comment = obj.comment;
    this.video.comments = [...this.video.comments, newOne];
    comment.value.comment = '';
  }
}
