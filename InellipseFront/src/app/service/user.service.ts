import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { User } from '../models/user';
import { Person } from '../models/person';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  public getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`http://localhost:8080/api/users/`);
  }
  public getUser(id: number): Observable<User> {
    return this.http.get<User>(`http://localhost:8080/api/users/${id}`);
  }
  public getSuggested(id: number): Observable<Person[]> {
    return this.http.get<Person[]>(
      `http://localhost:8080/api/people/getSuggested/${id}`
    );
  }

  public getPersonByVideo(id: number): Observable<Person> {
    return this.http.get<Person>(
      `http://localhost:8080/api/people/getPersonByVideo/${id}`
    );
  }
  public follow(obj: any): Observable<any> {
    return this.http.put<any>(`http://localhost:8080/api/users/follow`, obj);
  }
  public unfollow(obj: any): Observable<any> {
    return this.http.put<any>(`http://localhost:8080/api/users/unfollow`, obj);
  }
}
