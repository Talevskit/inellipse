import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Video } from '../models/videos';
import { PersonVideo } from '../models/personVideo';

@Injectable({
  providedIn: 'root',
})
export class VideoService {
  constructor(private http: HttpClient) {}

  public getVideos(): Observable<PersonVideo[]> {
    return this.http.get<PersonVideo[]>(`http://localhost:8080/api/videos/`);
  }
  public getAllVideos(id: any): Observable<PersonVideo[]> {
    return this.http.get<any[]>(`http://localhost:8080/api/videos/all/${id}`);
  }
  public getVideosWithAuthor(id: any): Observable<PersonVideo[]> {
    return this.http.get<PersonVideo[]>(
      `http://localhost:8080/api/videos/getVideosWithAuthor/${id}`
    );
  }
  public getVideo(id: any): Observable<Video> {
    return this.http.get<Video>(`http://localhost:8080/api/videos/${id}`);
  }
  public addComment(obj: any): Observable<any> {
    return this.http.put<any>(
      `http://localhost:8080/api/videos/addComment`,
      obj
    );
  }
}
