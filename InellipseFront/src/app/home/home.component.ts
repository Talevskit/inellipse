import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../models/user';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  public users!: User[];
  public user: User;
  public id: string;
  constructor(private UserService: UserService, private router: Router) {}

  ngOnInit(): void {
    this.getUsers();
  }

  public getUsers(): void {
    this.UserService.getUsers().subscribe((response: User[]) => {
      this.users = response;
      console.log(this.users);
    });
  }
  public getUser(id): void {
    this.router.navigateByUrl(`/user/${id}`);
  }
}
