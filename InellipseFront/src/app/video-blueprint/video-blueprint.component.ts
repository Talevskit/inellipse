import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PersonVideo } from '../models/personVideo';

@Component({
  selector: 'app-video-blueprint',
  templateUrl: './video-blueprint.component.html',
  styleUrls: ['./video-blueprint.component.css'],
})
export class VideoBlueprintComponent implements OnInit {
  // @Input() videos: PersonVideo[];
  @Input() video: PersonVideo;
  // @Input() personVideo: PersonVideo[];

  constructor(private router: Router) {}

  ngOnInit(): void {
    console.log(this.video);
  }
  public watchVideo(id) {
    this.router.navigateByUrl(`/videos/${id}`);
  }
}
