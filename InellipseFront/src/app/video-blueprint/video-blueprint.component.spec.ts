import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoBlueprintComponent } from './video-blueprint.component';

describe('VideoBlueprintComponent', () => {
  let component: VideoBlueprintComponent;
  let fixture: ComponentFixture<VideoBlueprintComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VideoBlueprintComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoBlueprintComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
