import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { VideosComponent } from './videos/videos.component';
import { VideoComponent } from './video/video.component';
import { FormsModule } from '@angular/forms';
import { NgxBootstrapIconsModule } from 'ngx-bootstrap-icons';
import { FollowingComponent } from './following/following.component';
import { SuggestedComponent } from './suggested/suggested.component';
import { LiveComponent } from './live/live.component';
import { VideoBlueprintComponent } from './video-blueprint/video-blueprint.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    UserComponent,
    VideosComponent,
    VideoComponent,
    FollowingComponent,
    SuggestedComponent,
    LiveComponent,
    VideoBlueprintComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgxBootstrapIconsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
