import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { PersonVideo } from '../models/personVideo';
import { User } from '../models/user';
import { Video } from '../models/videos';
import { VideoService } from '../service/video.service';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.css'],
})
export class VideosComponent implements OnInit {
  @Input() user: User;
  @Input() videos: PersonVideo[];
  @Input() personVideo: PersonVideo[];
  constructor(private VideoService: VideoService, private router: Router) {}

  ngOnInit(): void {}

  public getVideos(): void {
    this.VideoService.getVideos().subscribe((res: PersonVideo[]) => {
      this.videos = res;
      console.log(this.videos);
    });
  }

  public watchVideo(id) {
    this.router.navigateByUrl(`/videos/${id}`);
  }
}
