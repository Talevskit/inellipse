import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Person } from '../models/person';
import { User } from '../models/user';
import { UserService } from '../service/user.service';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-suggested',
  templateUrl: './suggested.component.html',
  styleUrls: ['./suggested.component.css'],
})
export class SuggestedComponent implements OnInit {
  // public persons: Person[];
  // id: number;
  // private sub: any;
  @Input() user: User;
  @Input() persons: Person[];
  @Output() userChange: EventEmitter<User> = new EventEmitter();
  @Output() personsChange: EventEmitter<Person[]> = new EventEmitter();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private UserService: UserService
  ) {}

  ngOnInit(): void {
    // console.log(this.user);
    // console.log(this.persons);
    // this.sub = this.route.params.subscribe((params) => {
    //   this.id = +params['id'];
    // });
    // this.getSuggested(this.id);
  }
  public getSuggested(id): void {
    this.UserService.getSuggested(id).subscribe((res: Person[]) => {
      this.persons = res;
      console.log(this.persons);
    });
  }

  public follow(id): void {
    let obj = {
      userId: localStorage.getItem('id'),
      peopleId: id,
    };
    this.UserService.follow(obj).subscribe((res: User) => {
      this.user = res;
    });
    let person: Person = this.persons.find((x) => x.id === id);
    this.user.following = [...this.user.following, person];

    this.persons = [...this.persons.filter((x) => x.id != id)];
    this.userChange.emit(this.user);
    this.personsChange.emit(this.persons);
  }
}
