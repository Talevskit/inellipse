import { Person } from './person';
import { Video } from './videos';

export class PersonVideo {
  person: Person;
  videos: Video;
}
