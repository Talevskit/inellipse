import { Video } from './videos';

export class Person {
  id: number;
  firstName: string;
  lastName: string;
  videos: Video[];
}
