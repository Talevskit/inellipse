import { Comment } from './comment';

export class Video {
  id: number;
  title: string;
  description: string;
  url: string;
  comments: Comment[];
}
