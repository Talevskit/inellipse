import { Person } from './person';

export class User {
  id: number;
  firstName: string;
  lastName: string;
  following: Person[];
}
