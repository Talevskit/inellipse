import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Person } from '../models/person';
import { User } from '../models/user';
import { UserService } from '../service/user.service';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-following',
  templateUrl: './following.component.html',
  styleUrls: ['./following.component.css'],
})
export class FollowingComponent implements OnInit {
  // public user: User;
  // id: number;
  // private sub: any;
  @Input() user: User;
  @Input() persons: Person[];
  @Output() personsChange: EventEmitter<Person[]> = new EventEmitter();
  @Output() userChange: EventEmitter<User> = new EventEmitter();

  constructor(
    private UserService: UserService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    // this.sub = this.route.params.subscribe((params) => {
    //   this.id = +params['id'];
    // });
    // this.getUser(this.id);
    console.log(this.user);
    console.log(this.persons);
  }
  public unfollow(id): void {
    console.log(this.user);
    console.log(this.persons);
    let obj = {
      userId: localStorage.getItem('id'),
      peopleId: id,
    };
    this.UserService.unfollow(obj).subscribe((res: User) => {
      this.user = res;
    });

    let person: Person = this.user.following.find((x) => x.id === id);
    this.persons = [...this.persons, person];

    this.user.following = [...this.user.following.filter((x) => x.id != id)];
    this.personsChange.emit(this.persons);
    this.userChange.emit(this.user);
  }

  public getUser(id): void {
    this.UserService.getUser(id).subscribe((res: User) => {
      this.user = res;
      console.log(this.user);
      localStorage.setItem('id', id);
      localStorage.setItem('firstName', this.user.firstName);
    });
  }
}
